docker exec -it kafka ./opt/kafka/bin/kafka-topics.sh --create --zookeeper zookeeper:2181  --topic Contacts --partitions 1  --replication-factor 1
docker exec -it kafka ./opt/kafka/bin/kafka-topics.sh --list --zookeeper zookeeper:2181


# Open Terminal 1
docker exec -it kafka ./opt/kafka/bin/kafka-console-producer.sh --broker-list 192.168.0.102:9092 --topic Contacts --property parse.key=true --property key.separator=:

# Open Terminal 2
docker exec -it kafka ./opt/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic Contacts --from-beginning