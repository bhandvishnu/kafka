from json import dumps
from time import sleep

from kafka import KafkaProducer

from config import TOPIC_CONTACTS

producer = KafkaProducer(
    bootstrap_servers=['localhost:9092'],
    api_version=(0, 10),
    retries=5,
    key_serializer=lambda x: x.encode('utf-8'),
    value_serializer=lambda x: dumps(x).encode('utf-8')
)

print("Successfully connected to kafka producer...")

for e in range(2):
    data = {'token': 'ya29.a0AfH6SMAhKnuPDCF601Ay4dA-QmZF7gJros57K5PMLFltdDwx1yNVWCQaeZBWaXu9SnvKSbzmoPFiEYaYMwTIU'
                     'jvdzOlXUVClvXMiQboOSz6Qk7Z_QIJyWi7C4y85vaLFFK5t_FSunfq3CJYdWy76cnl3aeIhkg'}
    future = producer.send(TOPIC_CONTACTS, key='bhandvishnu@gmail.com', value=data)
    result = future.get(timeout=60)
    print(f"{data} successfully sent to kafka topic.")
    sleep(0.5)

# https://kafka-python.readthedocs.io/en/master/usage.html
