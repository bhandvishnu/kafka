from json import loads

from kafka import KafkaConsumer

from config import TOPIC_CONTACTS, CONSUMER_CONTACTS_GRP

consumer = KafkaConsumer(
    TOPIC_CONTACTS,
    bootstrap_servers=['localhost:9092'],
    auto_offset_reset='latest',
    enable_auto_commit=True,
    auto_commit_interval_ms=1000,
    group_id=CONSUMER_CONTACTS_GRP,
    max_poll_records=5,
    max_poll_interval_ms=500000,
    key_deserializer=lambda x: x.decode('utf-8'),
    value_deserializer=lambda x: loads(x.decode('utf-8'))
)

print("Successfully connected to kafka consumer...")

# for message in consumer:
#     print(f"Consumed Value-> {message.key}: {message.value}")
#
# print(f"We have consumed all messages from topic {TOPIC_CONTACTS}")

while True:
    msgs = consumer.poll(timeout_ms=3000)
    for batch in msgs.items():
        unique_keys = []
        for msg in reversed(batch[1]):
            if msg.key not in unique_keys:
                print(msg)
                unique_keys.append(msg.key)
